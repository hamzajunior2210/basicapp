import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 5,
        itemBuilder: (context, rowNumber) {
          return Container(
            padding: EdgeInsets.all(25.0),
            child: Column(children: [
              Image.asset("assets/cocis.jpg"),
              Divider(color: Colors.blue),
              Text("Cocis", style: TextStyle(color: Colors.yellow))
            ]),
          );
        });
  }
}
