import 'package:flutter/material.dart';
import 'body.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Csc class",
              textScaleFactor: 2.0, style: TextStyle(color: Colors.green)),
          backgroundColor: Colors.yellow,
        ),
        body: Body(),
      ),
    );
  }
}
